package fr.cnam.foad.nfa035.fileutils.simpleaccess.streaming;

import java.io.*;

/**
 * ImageByteArrayFrame, donc qui implémente une image dans un tableau de byte.
 */
public class ImageByteArrayFrame extends AbstractImageFrameMedia{

    /**
     *
     * @param byteArrayOutputStream
     */
    public ImageByteArrayFrame(ByteArrayOutputStream byteArrayOutputStream) {
        super(byteArrayOutputStream);
    }

    /**
     *
     * @return
     * @throws IOException
     */

    @Override
    public OutputStream getEncodedImageOutput() throws IOException {
        return null;
    }

    @Override
    public InputStream getEncodedImageInput() throws IOException {
        return null;
    }
}
