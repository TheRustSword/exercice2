package fr.cnam.foad.nfa035.fileutils.simpleaccess.streaming;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class ImageDeserializerBase64StreamingImpl implements ImageStreamingDeserializer{
    /**
     *
     * @param deserializationOutput
     */
    public ImageDeserializerBase64StreamingImpl(ByteArrayOutputStream deserializationOutput) {
    }

    /**
     *
     * @param media
     * @throws IOException
     */
    @Override
    public void deserialize(Object media) throws IOException {

    }

    /**
     *
     * @return
     */
    @Override
    public OutputStream getSourceOutputStream() {
        return null;
    }

    /**
     *
     * @param media
     * @return
     * @throws IOException
     */
    @Override
    public InputStream getDeserializingStream(Object media) throws IOException {
        return null;
    }
}
