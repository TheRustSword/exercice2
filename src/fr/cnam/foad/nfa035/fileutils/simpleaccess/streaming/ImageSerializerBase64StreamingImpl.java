package fr.cnam.foad.nfa035.fileutils.simpleaccess.streaming;

import java.io.*;

/**
 * serialiser l'image du Base64
 */

public class ImageSerializerBase64StreamingImpl implements ImageStreamingSerializer {

    /**
     *
     * @param source
     * @param media
     * @throws IOException
     */
    @Override
    public void serialize(Object source, Object media) throws IOException {

    }

    /**
     *
     * @param media
     * @return
     * @throws IOException
     */
    @Override
    public OutputStream getSerializingStream(Object media) throws IOException {
        return null;
    }

    /**
     *
     * @param source
     * @return
     * @throws IOException
     */
    @Override
    public InputStream getSourceInputStream(Object source) throws IOException {
        return null;
    }
}
